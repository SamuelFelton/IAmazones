#include "GUI.h"
#include <chrono>
#include <SFML/Graphics.hpp>

#include "Game.h"
#include <chrono>

GUI::GUI() : window(sf::VideoMode(800, 800), "IAmazones v0.5",sf::Style::Close), selected(nullptr), IA(make_mcts(g,10000,1.5,8))
{
	std::chrono::milliseconds thinkPerFrame(100);
	window.setFramerateLimit(0);
	window.setVerticalSyncEnabled(false);
	sf::SoundBuffer buffBlack, buffWhite;
	if (!lightTexture.loadFromFile("assets/light_tile.jpg")) {
		std::cerr << "Erreur chargement light tile" << std::endl;
	}
	if (!darkTexture.loadFromFile("assets/dark_tile.jpg")) {
		std::cerr << "Erreur chargement dark tile" << std::endl;
	}
	if (!whiteQueenTexture.loadFromFile("assets/marine.png")) {
		std::cerr << "Erreur chargement white queen" << std::endl;
	}
	if (!blackQueenTexture.loadFromFile("assets/francois.png")) {
		std::cerr << "Erreur chargement black queen" << std::endl;
	}
	if (!arrowTexture.loadFromFile("assets/bars.png")) {
		std::cerr << "Erreur chargement fleche" << std::endl;
	}
	if (!winFont.loadFromFile("assets/AmaticSC-Regular.ttf")) {
		std::cerr << "Erreur chargement font" << std::endl;
	}
	winText.setFont(winFont);
	winText.setPosition(0.f, 350.f);
	winText.setCharacterSize(48);
	//Si marqué deprecated, utiliser setFillColor et OutlineColor
	winText.setColor(sf::Color::Green);
	//winText.setOutlineColor(sf::Color::Red);
	winText.setStyle(sf::Text::Bold);
	
	whiteQueen.setTexture(whiteQueenTexture);
	blackQueen.setTexture(blackQueenTexture);
	arrowSprite.setTexture(arrowTexture);

	arrowSprite.setScale(0.25f, 0.26f);
	whiteQueen.setScale(0.66f, 0.66f);
	blackQueen.setScale(0.277f, 0.277f);

	if (!buffBlack.loadFromFile("assets/ah.wav")) {
		std::cerr << "Ah n'a pu etre charge" << std::endl;
	}
	if (!buffWhite.loadFromFile("assets/jeanne.wav")) {
		std::cerr << "Jeanne n'a pu etre charge" << std::endl;
	}

	soundBlack.setBuffer(buffBlack);
	soundWhite.setBuffer(buffWhite);


	whiteQueen.setPosition(100, 100);
	for (int8_t i = 7; i >= 0; --i) {
		for (int8_t j = 7; j >= 0; --j) {
			sf::RectangleShape rs(sf::Vector2f(100.f,100.f));
			rs.setPosition(i * 100.f, j * 100.f);
			if ((i % 2) == (j % 2)) rs.setTexture(&lightTexture);
			else rs.setTexture(&darkTexture);
			tiles.push_back(rs);
		}
	}
	
	while (window.isOpen())
	{
#pragma omp parallel
        {
            try {
                IA.think(g, thinkPerFrame);
            } catch(char* c) {
                printf("%s\n",c);
            }
        }

		if(g.lost(Game::WHITE)) {
			std::cout << "LOST WHITE" << std::endl;
		}
		if(g.lost(Game::BLACK)) {
			std::cout << "LOST BLACK" << std::endl;
		}
		sf::Event event;
        if(g.whiteMustPlay) {
            std::cout << "IA JOUE" << std::endl;
            computerLastMove = IA.select_move();
            IA.last_move(g.moveIndex(computerLastMove));
            std::cout << g.moveIndex(computerLastMove) << std::endl;; 
            std::cout << (unsigned)computerLastMove << std::endl;
            g.make_move(computerLastMove);
			std::cout << g.movesPlayed << std::endl;

        }
		while (window.pollEvent(event))
		{

            if(!g.whiteMustPlay)
            {
                if (event.type == sf::Event::MouseButtonPressed) {

                    uint8_t x, y;
                    sf::Vector2i mousePos = sf::Mouse::getPosition(window);
                    getClickedTileIndex(mousePos.x, mousePos.y, x, y);
                    if (!selected)
                    {
                        Game::Player p = g.whiteMustPlay ? Game::WHITE : Game::BLACK;
                        if ((p == Game::WHITE && BIT_AT(g.bitboardWhite, y, x) || p == Game::BLACK && BIT_AT(g.bitboardBlack, y, x)) && g.canMove(y, x) && g.state != Game::GAME_OVER) {
                            selected = &getClickedTile(mousePos.x, mousePos.y);
                            highlitIndexes = g.possibleMoves(y, x);
                        }
                    }
                    else
                    {
                        uint8_t oldX, oldY;
                        getClickedTileIndex(selected->getPosition().x, selected->getPosition().y, oldX, oldY);
                        if (oldX == x && oldY == y && g.state == Game::SELECTING_QUEEN)
                        {
                            selected = nullptr;
                            highlitIndexes.clear();
                        }
                        else
                        {
                            Game::Player p = g.whiteMustPlay ? Game::WHITE : Game::BLACK;
                            if (g.moveIsLegal(oldY, oldX, y, x, p) && g.moveIsPossible(oldY, oldX, y, x))
                            {
                                if (g.state == Game::FIRING_ARROW) {
                                    selected = nullptr;
                                }
                                else {

                                    selected = &getClickedTile(mousePos.x, mousePos.y);

                                    if (p == Game::WHITE) soundWhite.play();
                                    else soundBlack.play();
                                }
                                humanLastMove = ((uint16_t) oldY << 9) | ((uint16_t) oldX << 6) | ((uint16_t) y << 3) | ((uint16_t) x);
                                IA.last_move(g.moveIndex(humanLastMove));
                                g.make_move(humanLastMove);
                                highlitIndexes = g.possibleMoves(y, x);
                                if (g.state == Game::SELECTING_QUEEN || g.state == Game::GAME_OVER) highlitIndexes.clear();


                                if (g.lost(Game::WHITE)) {
                                    winMessage = "Marine a perdu : un garde du corps n'est pas un conseiller";
                                    std::cout << winMessage << std::endl;
                                    g.state = Game::GAME_OVER;
                                    winText.setString(winMessage);
                                }

                                if (g.lost(Game::BLACK)) {
                                    winMessage = "Francois a perdu : les emplois fictifs l'ont ratrappe";
                                    std::cout << winMessage << std::endl;
                                    g.state = Game::GAME_OVER;
                                    winText.setString(winMessage);
                                }


                            }
                        }
                    }

                }
                if (event.type == sf::Event::Closed) window.close();
            }

		}

		window.clear();
		draw();
		window.display();
	}
}
void GUI::draw() 
{
	for (const sf::RectangleShape& s : tiles)
		window.draw(s);

	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j <8; ++j) {
			if (BIT_AT(g.bitboardWhite, i, j) > 0) {
				whiteQueen.setPosition(700 - j * 100, 700 - i * 100);
				window.draw(whiteQueen);
			}
			else if (BIT_AT(g.bitboardBlack, i, j) > 0) {
				blackQueen.setPosition(700 - j * 100, 700 - i * 100);
				window.draw(blackQueen);
			}
			else if (BIT_AT(g.bitboardArrows, i, j) > 0) {
				arrowSprite.setPosition(700 - j * 100, 700 - i * 100);
				window.draw(arrowSprite);
			}
		}
	}
	if (selected)
	{
		sf::RectangleShape r(*selected);
		r.setTexture(nullptr);
		r.setFillColor(sf::Color(0, 0, 200, 100));
		window.draw(r);
	}
	sf::RectangleShape high;
	high.setFillColor(sf::Color(0, 200, 0, 100));
	high.setSize(sf::Vector2f(100.f, 100.f));
	for (const auto& pos : highlitIndexes) {
		high.setPosition(700 - pos.second * 100, 700 - pos.first * 100);
		window.draw(high);
	}
	if (g.state == Game::GAME_OVER)
		window.draw(winText);
	
}
void GUI::getClickedTileIndex(unsigned x, unsigned y, uint8_t& resX, uint8_t&  resY) const
{
	resX = 7 - (x / 100);
	resY = 7 - (y / 100);
}
sf::RectangleShape& GUI::getClickedTile(unsigned x, unsigned y)
{
	return tiles[(7 - (x / 100)) * 8 + (7 - (y / 100))];
}
GUI::~GUI()
{
}
