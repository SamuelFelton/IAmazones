#include "mpi_util.hpp"

namespace MPI {
    int MASTER = 0;
    int RANK = 0;
    int WORLD_SIZE = 0;

    void init()
    {
        MPI_Init(NULL,NULL);
        MPI_Comm_size(MPI_COMM_WORLD, &WORLD_SIZE);
        MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
    }
    void setRank(int r) {RANK = r;}
    void setWorldSize(int w) {WORLD_SIZE = w;}

    bool isMaster() { return RANK == MASTER;}
}

