#pragma once

#include <stdint.h>
#include <iostream>
#include <vector>
#include <random>
#include <functional>
#include <algorithm>

class GUI;

#define BIT_AT(b, i, j) ((b) & ((uint64_t) 1  << ((i) * 8 + (j))))

#define TOT_MOVE(b) ((b) & 0x1F)
#define DIR_MOVE(b, i) (((b) >> (5 + (i) * 3)) & 0x7)


class Game {
public:
	using Player = uint8_t;
	using Move = uint16_t;

	static const Player WHITE = 0;
	static const Player BLACK = 1;


    typedef enum State {
        SELECTING_QUEEN,
        FIRING_ARROW,
        GAME_OVER
    } State;

    Game();

    ~Game();

    bool moveIsPossible(uint8_t fromI, uint8_t fromJ, uint8_t i, uint8_t j) const;

    bool moveIsLegal(uint8_t fromI, uint8_t fromJ, uint8_t i, uint8_t j, Player p) const;

    inline bool end_of_game() const
    {
        return (lost(WHITE) || lost(BLACK));
    }

    inline int value_for_current_player() const {
		return value(current_player());
    }

    inline uint8_t current_player() const {
        return whiteMustPlay ? WHITE : BLACK;
    }

    inline int value(uint8_t t) const {

        if(lost(t)){
			return -1;
		} else {
			return 1;
		}


    }


    inline int moveIndex(const Move& m)
    {
        auto movesVector = std::move(get_moves());
        auto it = std::find(movesVector.begin(),movesVector.end(),m);
        return it - movesVector.begin();
    }

    inline void playout(std::mt19937 &gen) {
        while (!end_of_game()) {
            auto movesVector = std::move(get_moves());
			int v = std::uniform_int_distribution<int>(0, movesVector.size() - 1)(gen);
			//std::cout << "chose between 0 and " << movesVector.size() << " : " << v << std::endl;
            make_move(movesVector[v]);
        }

    }



    inline bool won(Player player) const {
        return lost(player == 0 ? BLACK : WHITE);
    }

    std::string player_to_string(int player) const {
        return "AH !";
    }

    inline bool lost(Player p) const {
		const auto &queens = p == WHITE ? whiteQueens : blackQueens;
        for (const auto &q : queens) {
			const uint8_t i = uint8_t(q) >> 3;
			const uint8_t j = uint8_t(q) & 0x7;
            if (canMove(i,j)) return false;
        }


        return true;
    }

    inline void moveQueen(uint8_t fromI, uint8_t fromJ, uint8_t i, uint8_t j, Player p) {
        uint64_t &board = p == WHITE ? bitboardWhite : bitboardBlack;
        board &= ~((uint64_t) 1 << (fromI * 8 + fromJ));
        board |= ((uint64_t) 1 << (i * 8 + j));
        auto &queens = p == WHITE ? whiteQueens : blackQueens;
        std::ptrdiff_t pos = std::find(queens.begin(), queens.end(), fromI << 3 | fromJ) - queens.begin();
        queens[pos] = i << 3 | j;
    }

    inline void addArrow(uint8_t i, uint8_t j) { bitboardArrows |= (uint64_t) 1 << (i * 8 + j); }

    inline std::vector<Move> get_moves() const {
		std::vector<Move> movesVector;
        uint64_t emptyBoard = ~(bitboardArrows | bitboardBlack | bitboardWhite);

        if (state == SELECTING_QUEEN) {
            movesVector.reserve(60);
			const auto &queens = whiteMustPlay ? whiteQueens : blackQueens;
            for (int i = 0; i < 4; ++i) {
                const uint8_t origI = ((queens[i] >> 3) & 0x7), origJ = (queens[i] & 0x7);
                const uint8_t index = origI * 8 + origJ;
                const uint64_t queenBoard = (uint64_t) 1 << (index);
                setMovement(i, emptyBoard, queenBoard, origI, origJ, movesVector);
            }

        } else if (state == FIRING_ARROW) {
            movesVector.reserve(16);
            const uint8_t origI = selectedQueenI, origJ = selectedQueenJ;
            const uint64_t queenBoard = (uint64_t) 1 << (origI * 8 + origJ);
            setMovement(0, emptyBoard, queenBoard, origI, origJ, movesVector);
        }
        return movesVector;
    }


    int number_of_moves() const {
        std::cout << "CALLING NUMBER OF MOVES woops" << std::endl;
        return 0;
    }


    inline bool spaceIsOccupied(uint8_t i, uint8_t j) const {
        return i < 0 || j < 0 ||i > 7 || j > 7 || (BIT_AT(bitboardArrows, i, j)) ||
               (BIT_AT(bitboardWhite, i, j)) || (BIT_AT(bitboardBlack, i, j));
    }

    inline bool canMove(uint8_t i, uint8_t j) const {
        return !(spaceIsOccupied(i - 1, j) &&
                 spaceIsOccupied(i + 1, j) &&
                 spaceIsOccupied(i, j - 1) &&
                 spaceIsOccupied(i, j + 1) &&
                 spaceIsOccupied(i - 1, j - 1) &&
                 spaceIsOccupied(i - 1, j + 1) &&
                 spaceIsOccupied(i + 1, j - 1) &&
                 spaceIsOccupied(i + 1, j + 1));
    }


    void make_move(const Move &m) {
        uint8_t nI = (uint8_t)(m >> 3 & 0x7), nJ = (uint8_t)(m & 0x7);
        if (state == SELECTING_QUEEN) {
            uint8_t origI = (uint8_t)(m >> 9 & 0x7), origJ = (uint8_t)(m >> 6 & 0x7);

            moveQueen(origI, origJ, selectedQueenI = nI, selectedQueenJ = nJ, whiteMustPlay ? WHITE : BLACK);
            state = FIRING_ARROW;
        } else {
            addArrow(nI, nJ);
            state = SELECTING_QUEEN;
            whiteMustPlay = !whiteMustPlay;
        }
        ++movesPlayed;
    }

    uint16_t moves_played() const
    {
        return movesPlayed;
    }

    std::vector <std::pair<int8_t, int8_t>> possibleMoves(uint8_t i, uint8_t j) const;

    friend std::ostream &operator<<(std::ostream &os, const Game &g);

    friend GUI;

    uint64_t bitboardWhite;
    uint64_t bitboardBlack;
    uint64_t bitboardArrows;
    std::vector <uint8_t> whiteQueens, blackQueens;
    bool whiteMustPlay;
    uint8_t selectedQueenI, selectedQueenJ;
    uint16_t movesPlayed;
    State state;

private:

    void setMovement(int i, uint64_t emptyBoard, uint64_t queenBoard, int8_t origI, int8_t origJ, std::vector<Move>& movesVector) const;


};


