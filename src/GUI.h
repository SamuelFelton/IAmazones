#pragma once
#include <SFML/Graphics/RenderWindow.hpp>
#include "Game.h"
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Graphics/Text.hpp>
#include "mcts.hpp"

class GUI
{
public:
	GUI();
	~GUI();
	void draw();
	void getClickedTileIndex(unsigned x, unsigned y, uint8_t& resX, uint8_t&  resY) const;
	sf::RectangleShape& getClickedTile(unsigned x, unsigned y);
private:
	sf::RenderWindow window;
	Game g;
	std::vector<sf::RectangleShape> tiles;
	sf::Sprite whiteQueen;
	sf::Sprite blackQueen;
	sf::Sprite arrowSprite;


	sf::RectangleShape* selected;

	sf::Texture lightTexture;
	sf::Texture darkTexture;
	sf::Texture whiteQueenTexture;
	sf::Texture blackQueenTexture;
	sf::Texture arrowTexture;

	std::string winMessage;
	sf::Font winFont;
	sf::Text winText;
	sf::Sound soundBlack;
	sf::Sound soundWhite;
	
	mcts<Game> IA;
	int computerLastMove, humanLastMove;
	std::vector<std::pair<int8_t, int8_t>> highlitIndexes;
};

