// IAmazones.cpp�: d�finit le point d'entr�e pour l'application console.
//
#include <iostream>
#include "Game.h"
#include <omp.h>
#include <chrono>
#include "GUI.h"
#include "mcts.hpp"
#define OMP_NUM_THREADS 8

int main()
{
    int slave();
    srand(time(NULL));
    MPI::init();
    if(MPI::isMaster()) {
        std::cout << "AH " << MPI::WORLD_SIZE << std::endl;
        GUI();
    } else {
        slave();
    }


    MPI_Finalize();
	return 0;
}


int slave()
{
    Game g;
    auto ia = make_mcts(g,5000,1.2,8);
    std::chrono::milliseconds t(5000);

    while(true) {
        int move;
        MPI_Recv(&move,1,MPI_INT,MPI::MASTER,0,MPI_COMM_WORLD, NULL);
        if(move == -1) {
#pragma omp parallel
            ia.think(g, t);

            ia.send_root_children_data();
        } else {
            int index = g.moveIndex((Game::Move) move);
            g.make_move(move);
            ia.last_move(index);
        }
    }

}
