//
// Created by karam on 4/2/17.
//
#ifndef IAMAZONESV2MPI_MPI_UTIL_HPP
#define IAMAZONESV2MPI_MPI_UTIL_HPP
#include <mpi.h>

namespace MPI {
    extern int MASTER;
    extern int RANK;
    extern int WORLD_SIZE;
    void init();
    void setRank(int r);
    void setWorldSize(int w);

    bool isMaster();
}

#endif //IAMAZONESV2MPI_MPI_UTIL_HPP
