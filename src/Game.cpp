#include "Game.h"
#include <iostream>
#include <chrono>
#include <algorithm>

int8_t moveI[8] = { 0, 1, 1, 1, 0, -1, -1, -1 };
int8_t moveJ[8] = { 1, 1, 0, -1, -1, -1, 0, 1 };
Game::Game() : bitboardBlack(2594215222365388800), bitboardWhite(8454180), bitboardArrows(0),
whiteMustPlay(true), state(SELECTING_QUEEN), movesPlayed(0),
whiteQueens(4), blackQueens(4)
{
	whiteQueens[0] = 2;
	whiteQueens[1] = 5;
	whiteQueens[2] = (uint8_t)2 << 3;
	whiteQueens[3] = (uint8_t)2 << 3 | (uint8_t) 7;

	blackQueens[0] = (uint8_t)7 << 3 | (uint8_t)5;
	blackQueens[1] = (uint8_t)7 << 3 | (uint8_t)2;
	blackQueens[2] = (uint8_t)5 << 3 ;
	blackQueens[3] = (uint8_t)5 << 3 | (uint8_t)7;


}
bool Game::moveIsPossible(uint8_t fromI, uint8_t fromJ, uint8_t i, uint8_t j) const
{
	if (fromI == i && fromJ == j) return false;
	int stepI = 0;
	int stepJ = 0;
	if (fromI < i) {
		stepI = 1;
	}
	else if (fromI > i) {
		stepI = -1;
	}
	if (fromJ < j) {
		stepJ = 1;
	}
	else if (fromJ > j) {
		stepJ = -1;
	}
	int8_t ii =  fromI + stepI;
	int8_t jj =  fromJ + stepJ;
	while (ii != i + stepI || jj != j + stepJ) {
		//std::cout << (int)ii << std::endl;
		if ((BIT_AT(bitboardArrows, ii, jj)) > 0 || BIT_AT(bitboardBlack, ii, jj) > 0 || BIT_AT(bitboardWhite, ii, jj) > 0) {
			return false;
		}
		ii += stepI;
		jj += stepJ;
	}

	return true;
}

bool Game::moveIsLegal(uint8_t fromI, uint8_t fromJ, uint8_t i, uint8_t j, Game::Player p) const
{
	if (p == WHITE && BIT_AT(bitboardWhite, fromI, fromJ) == 0) return false;
	if (p == BLACK && BIT_AT(bitboardBlack, fromI, fromJ) == 0) return false;

	if (fromI == i && fromJ != j) return true;
	if (fromJ == j && fromI != i) return true;
	int diagValue = std::abs((fromI * 8 + fromJ) - (i * 8 + j));
	if (diagValue % 7 == 0 || diagValue % 9 == 0) return true;
	return false;
}




std::vector<std::pair<int8_t, int8_t>> Game::possibleMoves(uint8_t i, uint8_t j) const
{
	std::vector<std::pair<int8_t, int8_t>> result;
	result.reserve(28);
	for (int ah = 0; ah < 8; ++ah)
	{
		int8_t ii = (int8_t)i + moveI[ah], jj = (int8_t)j + moveJ[ah];
		while (!spaceIsOccupied(ii, jj)) {
			result.push_back(std::make_pair(ii, jj));
			ii += moveI[ah];
			jj += moveJ[ah];
		}
	}
	return result;
}


Game::~Game()
{
}





void Game::setMovement(int i, uint64_t emptyBoard, uint64_t queenBoard, int8_t origI, int8_t origJ, std::vector<Move>& movesVector) const
{


	int8_t nI, nJ;

	//gauche
	nJ = origJ + 1;
	while ((nJ < 8) && (emptyBoard & (queenBoard << (nJ - origJ)))) {
		movesVector.push_back(((uint16_t(origI) << 9) | (uint16_t(origJ) << 6) | (uint16_t(origI) << 3) | nJ));
		++nJ;
	}

	//diag haut gauche
	nJ = origJ + 1, nI = origI + 1;
	while (nI < 8 && nJ < 8 && (emptyBoard & (queenBoard << (9 * (nJ - origJ))))) {
		movesVector.push_back(((uint16_t(origI) << 9) | (uint16_t(origJ) << 6) | ((uint16_t)nI << 3)| nJ));
		++nJ; ++nI;
	}
	//haut
	nI = origI + 1;
	while ((nI < 8) && (emptyBoard & (queenBoard << (8 * (nI - origI))))) {
		movesVector.push_back(((uint16_t(origI) << 9) | (uint16_t(origJ) << 6) | ((uint16_t)nI << 3) | origJ));
		++nI;
	}

	//hautDroit
	nJ = origJ - 1, nI = origI + 1;
	while (nI < 8 && nJ >= 0 && (emptyBoard & (queenBoard << (7 * (nI - origI))))) {
		movesVector.push_back(((uint16_t(origI) << 9) | (uint16_t(origJ)) << 6 | ((uint16_t)nI << 3) | nJ));
		--nJ; ++nI;
	}


	//droite
	nJ = origJ - 1;
	while ((nJ >= 0) && (emptyBoard & (queenBoard >> (origJ - nJ)))) {
		movesVector.push_back(((uint16_t(origI) << 9) | (uint16_t(origJ) << 6) | ((uint16_t)origI << 3) | nJ));
		--nJ;
	}


	//diag bas droite
	nJ = origJ - 1, nI = origI - 1;
	while (nI >= 0 && nJ >= 0 && (emptyBoard & (queenBoard >> (9 * (origJ - nJ))))) {
		//std::cout << (unsigned)(origJ - nJ) << std::endl;
		movesVector.push_back(((uint16_t(origI) << 9) | (uint16_t(origJ) << 6) | ((uint16_t)nI << 3) | nJ));
		--nJ; --nI;
	}


	//bas
	nI = origI - 1;
	while (nI >= 0 && (emptyBoard & (queenBoard >> (8 * (origI - nI))))) {
		movesVector.push_back(((uint16_t(origI) << 9) | (uint16_t(origJ) << 6) | ((uint16_t)nI << 3) | origJ));
		--nI;
	}

	//diagBasGauche
	nJ = origJ + 1, nI = origI - 1;
	while (nI >= 0 && nJ < 8 && (emptyBoard & (queenBoard >> (7 * (origI - nI))))) {
		movesVector.push_back(((uint16_t(origI) << 9) | (uint16_t(origJ) << 6) | ((uint16_t)nI << 3) | nJ));
		++nJ; --nI;
	}

}

std::ostream& operator<<(std::ostream& os, const Game& g)
{
	for (int i = 7; i >= 0; --i) {
		for (int j = 7; j >= 0; --j) {
			if (BIT_AT(g.bitboardWhite, i, j) > 0) {
				os << "W ";
			}
			else if (BIT_AT(g.bitboardBlack, i, j) > 0) {
				os << "B ";
			}
			else if (BIT_AT(g.bitboardArrows, i, j) > 0) {
				os << "A ";
			}
			else {
				os << "~ ";
			}
		}
		os << std::endl;
	}
	return os;
}
