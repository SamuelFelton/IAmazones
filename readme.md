Projet du module d'ouverture d'intelligence artificielle de l'INSA Rennes,2016-2017

Crée par Guillaume COURTET et Samuel FELTON
Encadrant: Pascal GARCIA

Code du MCTS fourni par Pascal GARCIA

Dépendances:
 -Linux
 -OpenMP
 -SFML >= 2.4
 -MPI
 
pour lancer avec MPI :
 -dans le répertoire racine: make && mpiexec -n nbMachines -f fichierContenantlesMachines ./bin/main
sans MPI:
 -make && ./bin/main
